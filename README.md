## aosp_RMX2020-userdebug 12 SP1A.210812.016 eng.root.20211005.084715 test-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2020
- Brand: realme
- Flavor: aosp_RMX2020-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.root.20211005.084715
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/aosp_RMX2020/RMX2020:12/SP1A.210812.016/root10050845:userdebug/test-keys
- OTA version: 
- Branch: aosp_RMX2020-userdebug-12-SP1A.210812.016-eng.root.20211005.084715-test-keys
- Repo: realme_rmx2020_dump_30265


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
